//
//  ViewController.swift
//  Seafood Watson
//
//  Created by iulian david on 8/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import VisualRecognitionV3
import SVProgressHUD


class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cameraButton: UIBarButtonItem!

    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
    }

    @IBAction func cameraTapped(_ sender: UIBarButtonItem) {
        
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }

}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Did not select an image")
        }
        
        imageView.image = image
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        let visualRecognition = VisualRecognition(apiKey: o.reveal(key: Constants.WATSON_API_KEY), version: o.reveal(key: Constants.API_VERSION))
        
        let imageData = UIImageJPEGRepresentation(image, 0.01)
        
        guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return
        }
        
        let fileURL = documentsUrl.appendingPathComponent("tempImage.jpg")
        
        try? imageData?.write(to: fileURL)
        SVProgressHUD.show()
        visualRecognition.classify(imageFile: fileURL) { (classifiedImages) in
            guard let imageResponse = classifiedImages.images.first else {
                fatalError("cannot retrieve any Image from Watson")
            }
            guard let firstResult = imageResponse.classifiers.first else {
                fatalError("no classifier from Watson")
            }
            
            let filteredResults = firstResult.classes.filter {$0.classification.contains("hotdog")}.flatMap({return $0.classification})
            
           
            let allResults = firstResult.classes.flatMap({return $0.classification})
   
            if filteredResults.count > 0 {
                DispatchQueue.main.async {
                    self.navigationItem.title = "HOTDOG!"
                }
                
            } else {
                DispatchQueue.main.async {
                self.navigationItem.title = "NOT A HOTDOG!"
                }
            }
            SVProgressHUD.dismiss()
            print(allResults)
            
        }
    }
}

