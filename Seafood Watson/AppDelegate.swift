//
//  AppDelegate.swift
//  Seafood Watson
//
//  Created by iulian david on 8/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

let o = Obfuscator(withSalt: [AppDelegate.self, NSObject.self, NSString.self])

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        print(o.bytesByObfuscatingString(string: "2017-08-10"))
        return true
    }


}

